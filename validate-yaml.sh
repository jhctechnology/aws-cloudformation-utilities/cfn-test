#!/bin/bash
ERROR_COUNT=0;

echo "Lint yaml files..."

# Loop through the YAML templates in this repository
for YAMLFILE in $(find . -regex ".*y[a]*ml"); do 

    # Validate the template with yamllint
    ERRORS=$(yamllint -d "{extends: relaxed, rules: {line-length: {max: 250}}}" $YAMLFILE); 
    if [ "$?" -gt "0" ]; then 
        ((ERROR_COUNT++));
        echo "[fail] $ERRORS";
    else 
        echo "[pass] $YAMLFILE";
    fi; 
    
done; 

echo "$ERROR_COUNT  yaml lint error(s)"; 
if [ "$ERROR_COUNT" -gt 0 ]; 
    then exit 1; 
fi
